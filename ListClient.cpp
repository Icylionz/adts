#include <iostream>
#include "List.h"

using namespace std;

int main()
{

 List L1, L2; //Declare two list objects, L1 and L2


 cout<< "Welcome to my List ADT client"<<endl<<endl;

 //Do some stuff with L1, L2, ... (Eg. cout<<L1.size(); )
	cout<<"Elements in L1:"<<L1.size()<<endl;
	L1.insert(10,1);
	L1.insert(9,2);
	L1.insert(8,3);
	cout<<"Elements in L1:"<<L1.size()<<endl;
	cout<<"Elements in L1 position 2:"<<L1.get(2)<<endl;
	L1.remove(2);
	cout<<"Elements in L1 position 2:"<<L1.get(2)<<endl;
	L1.clear();
	cout<<"Elements in L1:"<<L1.size()<<endl;
	cout<<"Elements in L1 position 2:"<<L1.get(2)<<endl;
 // ...
 

}
