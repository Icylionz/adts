#include "StackV.h"

int Stack::size()
{
	return data.size();
}

void Stack::push(int val)
{
	data.push_back(val);
}

void Stack::pop()
{
	data.pop_back();
}

int Stack::top()
{	
	return data[data.size()-1];
}
void Stack::clear()
{
	while(Stack::size()>0)
		Stack::pop();
}
