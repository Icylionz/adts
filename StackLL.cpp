#include "StackLL.h"
#include <iostream>
#include <stdexcept>
class Stack::Node //self-referential Node class
{
	public:
	   int data = 0;
	   Node* link = nullptr;
	   //link is a data member which is a pointer 
	   //to an object of the same type (i.e. Node)
	
};//end Node class definition (can only be seen by the List class)

Stack::~Stack()
{
	Stack::clear(); //d'tor
}


int Stack::size()
{
	return num_elements;
}
void Stack::push(int val)
{
	Node* newPtr = new Node{val};
	
	if(Stack::size()==0)
	{
	  newPtr->link = frontPtr;
	  frontPtr = newPtr;
	 }
	else
	 {  
		int k=Stack::size()+1;
		Node* tmpPtr = frontPtr;
		int loc = 1; 
	  
	    while( loc != k-1) //get pointer to (k-1)th node
	     {
			tmpPtr = tmpPtr->link;
			loc++;
	     }
	
		newPtr->link = tmpPtr->link;
		tmpPtr->link = newPtr;  
      }//end else

     num_elements++;
}
void Stack::pop()
{
	int k=Stack::size();
	if (k == 0)//if the location is invalid 
	     throw out_of_range("List::remove(" +to_string(k)+") failed. (valid indices are 1 to "+to_string(num_elements)+")");//throw an "out_of_range" exception
	
	Node* delPtr;
	
	if(k == 1)
	{
	  delPtr = frontPtr;
	  frontPtr = frontPtr->link;
	 }
	 else
	 {
	    Node* tmpPtr = frontPtr;
		
	    int loc = 1;
            
            while(loc != k-1)//get pointer to (k-1)th node
	    {
	       tmpPtr = tmpPtr->link;
		loc++;
	    }
	
	    delPtr = tmpPtr->link;
	    tmpPtr->link = delPtr->link;
	  }
	
	delete delPtr;
	num_elements--;
}

int Stack::top()
{
	int k = Stack::size();
	if (k <=0)//if the location is invalid 
			throw out_of_range("List::remove(" +to_string(k)+") failed. (valid indices are 1 to "+to_string(num_elements)+")");//throw an "out_of_range" exception
	
		if(k==1)
			return frontPtr->data;
		else
		{
			
			Node* tmpPtr=frontPtr;
			for(int i=0;i<k-1;i++)
				tmpPtr=tmpPtr->link;
			return tmpPtr->data;
			
		}
	
}
void Stack::clear()
{	
	   while(Stack::size()>0)
			Stack::pop();
}
